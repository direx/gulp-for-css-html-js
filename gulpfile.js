// gulp plugins
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    es = require('event-stream'),
    minifyCSS = require('gulp-minify-css'),
    rename = require('gulp-rename');

// стили
gulp.task('sass', function () {
    return gulp.src('./css/**/*.scss')
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./css/'));
});


// изображения
gulp.task('images', function () {
    return es.concat(
        // png
        gulp.src('./img/**/*.png')
            .pipe(imagemin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('./img')),
        // jpg
        gulp.src('./img/**/*.jpg')
            .pipe(imagemin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('./img')),
        // gif
        gulp.src('./img/**/*.gif')
            .pipe(imagemin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('./img'))
    );
});

// Сборка
gulp.task('build', ['sass', 'images']);
