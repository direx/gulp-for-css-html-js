// event: страница полностью загружена
window.onload = function () {
    // скроем загрузчик
    Preloader.hide();
};

/**
 * Загрузчик
 */
var Preloader = new Object({
    // Таймаут перед тем как загрузчик будет скрыт (мс)
    hideTimeOut: 500,

    //  Показать загрузчик
    show: function () {
        var $self = this;
        var cBody = 0;

        // создаем новый div для предзагрузчика
        var newLoaderDiv = document.createElement('div');
        newLoaderDiv.className = 'preloader';
        newLoaderDiv.setAttribute('id', 'preloader');

        var tBody = setInterval(function () {

            if (document.body == null) {
                cBody++;

                if (cBody == 200) {
                    clearInterval(tBody);
                }
            } else {
                clearInterval(tBody);
                // добавляем элемент в конец DOMа
                document.body.appendChild(newLoaderDiv);
            }
        }, 1);
    },

    // Скрыть загрузчик
    hide: function (timeout) {
        var $self = this;
        var timeout = timeout || $self.hideTimeOut;
        var laoder = $('#preloader');

        // скрываем эелемент
        $(laoder).fadeOut(timeout);
        // удаляем элемент
        setTimeout(function () {
            $(laoder).remove();
        }, timeout);
    },
});

Preloader.show();